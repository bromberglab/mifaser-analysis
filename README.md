# mi-faser analysis #
### A collection of tools and scripts for downstream analysis of mi-faser results. ###

***mi-faser* Web Service:** https://bromberglab.org/services/mifaser/

***mi-faser* Docker image:** https://hub.docker.com/r/bromberglab/mifaser

## Pre-Requirements ##

**Dependencies**

* Python >= 3.6
* R >= 3.6

## Usage ##

**Compare annotation results**
* `R/compare_annotations/mifaser_vs_prokka.Rmd`

## License ##

This project is licensed under [NPOSL-3.0](http://opensource.org/licenses/NPOSL-3.0).

## Citation ##

If you use *mi-faser* in published research, please cite:

Zhu, C., Miller, M., Marpaka, S., Vaysberg, P., Rühlemann, M. C., Wu, G. H. F.-A., . . . Bromberg, Y. *(2017)*. Functional sequencing read annotation for high precision microbiome analysis. Nucleic Acids Res. [doi:10.1093/nar/gkx1209](https://academic.oup.com/nar/advance-article/doi/10.1093/nar/gkx1209/4670955)


## About ##

*mi-faser* is developed by Chengsheng Zhu and Maximilian Miller. Feel free to contact us for support: [services@bromberglab.org](mailto:services@bromberglab.org).
